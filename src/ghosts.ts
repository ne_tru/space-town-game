import { BaseCSW } from './base/baseCsw';

type Ghosts = BaseCSW[];

export type { Ghosts };
